<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->auth = $this->load->database('auth', TRUE);
    }

    public function getSlides()
    {
        return $this->db->select('*')
                ->order_by('id', 'ASC')
                ->get('fx_slides');
    }

    public function getDiscordInfo()
    {
		
    	$invitation = $this->config->item('discord_inv');
    	$discord = file_get_contents('http://discordapp.com/api/v6/invite/SygPvbw?with_counts=true');
    	$vars = json_decode($discord, true);
    	return $vars;
    }

	
	function curl($url) {
        $ch = curl_init($url); // Inicia sesión cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // Configura cURL para devolver el resultado como cadena
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Configura cURL para que no verifique el peer del certificado dado que nuestra URL utiliza el protocolo HTTPS
        $info = curl_exec($ch); // Establece una sesión cURL y asigna la información a la variable $info
        curl_close($ch); // Cierra sesión cURL
        return $info; // Devuelve la información de la función
    }
	
    public function updateInstallation()
    {
        $this->db->set('status', '0')
                ->where('id', '20')
                ->update('fx_modules');
    }
}
